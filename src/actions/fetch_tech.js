

export function fetchTech(){
    //return the actual action to do
        return function(dispatch){
            fetch("http://newsapi.org/v1/articles?source=the-verge&sortBy=top&apiKey=69c2f507be1c4822924ddd5f7e2d9695")
            .then(res => {
                return res.json();
                
            })
            .then(res => {
             // console.log(res)
              dispatch({type:"FETCH_TECH", payload: res.articles});
            })
            .catch(err => {
                console.log(err);
            })
      
         }
      
}